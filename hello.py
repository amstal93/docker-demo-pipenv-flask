from flask import Flask
import settings
import os

app = Flask(__name__)

@app.route('/')
def index():
  name = os.getenv('NAME','World')
  return f"Hello {name}"

